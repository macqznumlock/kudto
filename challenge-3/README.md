# Instructions

This one will get you fiddling with margins and padding!

### Hints:

- You'll need to set a fixed width for your wrap, and then float the navigation div.
- You may also want to fiddle with widths/margins of the navigation and content divisions!


